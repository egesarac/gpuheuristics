#!/usr/bin/python
import os
import sys

cmdTemplate = "./heuristics_%s %d %d %d 100 %d 8000"

def tester(stateSizes, alphabetSizes, experimentSize, algorithm, coefficient):
    for s in stateSizes:
        for p in alphabetSizes:
            print "stateSize:", s, "alphabetSize:", p
            sys.stdout.write("00%")
            for expID in range(1, experimentSize + 1):
                cmd = cmdTemplate % (algorithm, s, p, expID * 2, coefficient)
                retval = os.system(cmd + " > /dev/null")
                if retval != 0:
                    exit(1)
                    
                percentage = (expID) / (experimentSize / 100.0)
                sys.stdout.write("\r%02d" % percentage)
                sys.stdout.write("%")
                sys.stdout.flush()
            print


def main():
    stateSizes1 = [1024, 2048, 4096, 8192]
    alphabetSizes = [2, 8, 32]
    experimentSize = 5
    """stateSizes1 = [256]
    alphabetSizes = [2]"""
    
    for algo in ["pc", "nopc"]:
        print "algorithm:", algo
        for coef in [1, 25, 50, 100]:
            print "coefficient:", coef  
            tester(stateSizes1, alphabetSizes, experimentSize, algo, coef)
    
if __name__ == "__main__":
    main()
