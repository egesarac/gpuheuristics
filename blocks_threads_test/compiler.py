#!/usr/bin/python
import os
import sys

prev_tpb = 128

def compiler(level, tpb):
    naive = open("naive.h").read()
    findThread = '#define THREAD_PER_BLOCK ' + prev_tpb
    writeThread = '#define THREAD_PER_BLOCK ' + tpb
    naive = naive.replace(findThread, writeThread)        
    fnaive = open("naive.h", 'w')
    fnaive.write(naive)
    fnaive.close()
            
    make = open("Makefile").read()
    findName = level + "_" + prev_tpb
    writeName = level + "_" + tpb
    make = make.replace(findName, writeName)
    fmake = open("Makefile", "w")
    fmake.write(make)
    fmake.close()
            
    cmd = "make -B"
    retval = os.system(cmd + " > /dev/null")
    if retval != 0:
        exit(1)
            
            
def main():
    tpbList = [128, 256, 512]
    
    for l in range(2):
        for t in tpbList:
            os.chdir("/home/esarac/costHeuristics/all-ThreadLevel-v" + str(l) + "/" + str(t))
            compiler("t" + str(l), t)
            prev_tpb = t
            
    for l in range(4):
        for t in tpbList:
            os.chdir("/home/esarac/costHeuristics/all-WarpLevel-v" + str(l) + "/" + str(t))
            compiler("t" + str(l), t)
            prev_tpb = t

if __name__ == "__main__":
    main()