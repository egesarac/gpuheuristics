#!/usr/bin/python
import os
import sys

            
def main():
    tpbList = [128, 256, 512]
    
    for l in range(2):
        for t in tpbList:
            os.chdir("/home/esarac/costHeuristics/experiments/all-ThreadLevel-v" + str(l) + "/" + str(t))
            cmd = "make -B"
            retval = os.system(cmd + " > /dev/null")
            if retval != 0:
                exit(1)
            
    for l in range(4):
        for t in tpbList:
            os.chdir("/home/esarac/costHeuristics/experiments/all-WarpLevel-v" + str(l) + "/" + str(t))
            cmd = "make -B"
            retval = os.system(cmd + " > /dev/null")
            if retval != 0:
                exit(1)
                
if __name__ == "__main__":
    main()