#!/usr/bin/python
import os
import sys

cmdTemplate = "./heuristics %s %s %s 100 1 8000"

def tester(suite, experimentSize):
    for fsm in suite:
        print "machine:", fsm
        sys.stdout.write("00%")
        for expID in range(1, experimentSize + 1):
            fsmList = fsm.split("_")
            cmd = cmdTemplate % (fsmList[0], fsmList[1], fsm)
            retval = os.system(cmd + " > /dev/null")
            if retval != 0:
                exit(1)
                    
            percentage = (expID) / (experimentSize / 100.0)
            sys.stdout.write("\r%02d" % percentage)
            sys.stdout.write("%")
            sys.stdout.flush()
        print


def main():
    suite = [line.rstrip() for line in open("mealySuite.txt")]
    experimentSize = 20

    for algo in ["org", "nopc", "pc"]:
        os.chdir("/home/esarac/public/sequential/" + algo)
        print "algorithm:", algo
        tester(suite, experimentSize)
    
if __name__ == "__main__":
    main()
