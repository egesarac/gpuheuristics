#!/usr/bin/python
import os
import sys

            
def main():
    tpbList = [128, 256, 512]
    
    for l in range(2):
        for t in tpbList:
            os.chdir("/home/esarac/costHeuristics/experiments_titan/all-ThreadLevel-v" + str(l) + "/" + str(t))
            
            heuristics = open("heuristics.cu").read()
            findDevice = 'cudaSetDevice(1);'
            writeDevice = 'cudaSetDevice(3);'
            heuristics = heuristics.replace(findDevice, writeDevice)      
            fheuristics = open("heuristics.cu", 'w')
            fheuristics.write(heuristics)
            fheuristics.close()
            
            naive = open("naive.h").read()
            findMem = 'memory = 4035000000'
            writeMem = 'memory = 12187000000 * 0.7'
            naive = naive.replace(findMem, writeMem)
            fnaive = open("naive.h", "w")
            fnaive.write(naive)
            fnaive.close()
            
            cmd = "make -B"
            retval = os.system(cmd + " > /dev/null")
            if retval != 0:
                exit(1)
            
    for l in range(4):
        for t in tpbList:
            os.chdir("/home/esarac/costHeuristics/experiments_titan/all-WarpLevel-v" + str(l) + "/" + str(t))
            
            heuristics = open("heuristics.cu").read()
            findDevice = 'cudaSetDevice(1);'
            writeDevice = 'cudaSetDevice(3);'
            heuristics = heuristics.replace(findDevice, writeDevice)      
            fheuristics = open("heuristics.cu", 'w')
            fheuristics.write(heuristics)
            fheuristics.close()
            
            naive = open("naive.h").read()
            findMem = 'memory = 4035000000'
            writeMem = 'memory = 12187000000 * 0.7'
            naive = naive.replace(findMem, writeMem)
            fnaive = open("naive.h", "w")
            fnaive.write(naive)
            fnaive.close()
            
            cmd = "make -B"
            retval = os.system(cmd + " > /dev/null")
            if retval != 0:
                exit(1)
                
if __name__ == "__main__":
    main()