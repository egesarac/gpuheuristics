#!/usr/bin/python
import os
import sys

cmdTemplate = "./heuristics %d %d %d %d %d %d"
for c in [1, 25, 50, 100]:
    for t in [0.1, 0.25, 0.5, 0.75, 0.9]:
        for i in range(1,6):
            print "coef:", c, "threshold:", t, "expID:", i
            cmd = cmdTemplate % (500, 32, i*2, 500*t, c, 8000)
            retval = os.system(cmd + " > /dev/null")
            if retval != 0:
                exit(1)