#!/usr/bin/python
import os
import sys

cmdTemplate = "./heuristics_%s %d cerny 100 1 8000"

def tester(stateSizes, experimentSize, algorithm):
    for s in stateSizes:
        print "stateSize:", s
        sys.stdout.write("00%")
        for expID in range(1, experimentSize + 1):
            cmd = cmdTemplate % (algorithm, s)
            retval = os.system(cmd + " > /dev/null")
            if retval != 0:
                exit(1)
                    
            percentage = (expID) / (experimentSize / 100.0)
            sys.stdout.write("\r%02d" % percentage)
            sys.stdout.write("%")
            sys.stdout.flush()
        print


def main():
    stateSizes = [32,64,128]
    experimentSize = 50

    for algo in ["org", "pc", "nopc"]:
        print "algorithm:", algo
        tester(stateSizes, experimentSize, algo)
    
if __name__ == "__main__":
    main()
