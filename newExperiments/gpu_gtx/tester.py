#!/usr/bin/python
import os
import sys

cmdTemplate = "./heuristics_%s %d cerny"

def tester(stateSizes, experimentSize, algorithm):
    for s in stateSizes:
        print "stateSize:", s
        sys.stdout.write("00%")
        for expID in range(1, experimentSize + 1):
            cmd = cmdTemplate % (algorithm, s)
            retval = os.system(cmd + " > /dev/null")
            if retval != 0:
                exit(1)
                    
            percentage = (expID) / (experimentSize / 100.0)
            sys.stdout.write("\r%02d" % percentage)
            sys.stdout.write("%")
            sys.stdout.flush()
        print


def main():
    stateSizes = [32, 64, 128]
    experimentSize = 20
    algorithms = ["sp"] #["sp", "pl", "fr", "cr"]

    print "T0"
    os.chdir("/home/esarac/besh/gpu_gtx/all-ThreadLevel-v0")
    for algo in algorithms:
        print "algorithm:", algo
        tester(stateSizes, experimentSize, algo)
    
    print "T1"
    os.chdir("/home/esarac/besh/gpu_gtx/all-ThreadLevel-v1")
    for algo in algorithms:
        print "algorithm:", algo
        tester(stateSizes, experimentSize, algo)

    print "W0"
    os.chdir("/home/esarac/besh/gpu_gtx/all-WarpLevel-v0")
    for algo in algorithms:
        print "algorithm:", algo
        tester(stateSizes, experimentSize, algo)

    print "W1"
    os.chdir("/home/esarac/besh/gpu_gtx/all-WarpLevel-v1")
    for algo in algorithms:
        print "algorithm:", algo
        tester(stateSizes, experimentSize, algo)

    print "W3"
    os.chdir("/home/esarac/besh/gpu_gtx/all-WarpLevel-v3")
    for algo in algorithms:
        print "algorithm:", algo
        tester(stateSizes, experimentSize, algo)  

if __name__ == "__main__":
    main()