#!/usr/bin/python
import os
import sys

cmdTemplate = "./heuristics_%s %d %s 100 1 8000"

def tester(stateSizes, experimentSize, ssaType, algorithm):
    for s in stateSizes:
        if ssaType == "bactrian":
                s = s - 1
        if ssaType == "doubleCerny":
                s = s / 2
        print "stateSize:", s
        sys.stdout.write("00%")
        for expID in range(1, experimentSize + 1):
            cmd = cmdTemplate % (algorithm, s, ssaType)
            retval = os.system(cmd + " > /dev/null")
            if retval != 0:
                exit(1)
                    
            percentage = (expID) / (experimentSize / 100.0)
            sys.stdout.write("\r%02d" % percentage)
            sys.stdout.write("%")
            sys.stdout.flush()
        print


def main():
    stateSizes = [64,128,256,512]
    experimentSize = 20

    for ssaType in ["bactrian", "dromedary", "fix5", "fix4", "fix3", "doubleCerny"]:
        for algo in ["org", "pc", "nopc"]:
            print "automata:", ssaType
            print "algorithm:", algo
            tester(stateSizes, experimentSize, ssaType, algo)
    
if __name__ == "__main__":
    main()
