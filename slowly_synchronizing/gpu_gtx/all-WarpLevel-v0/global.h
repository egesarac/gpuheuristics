#ifndef _GLOBAL_
#define _GLOBAL_

#include <iostream>
#include <cmath>
using namespace std;

#define TIMER
//#define DEBUG
//#define AUTOMATA_ANALYSIS 1
#ifdef TIMER
#define LEVEL_ANALYSIS 4
#define TIME_ANALYSIS 1 //#define TIMER macro is required for time analysis
#endif
#define DATA_ANALYSIS 2
#define LOG_LEVEL TIME_ANALYSIS


#define DIV(X, Y) (1+((X-1)/Y))

#ifdef LOG_LEVEL
ofstream out;
ofstream aout; //for saving the automaton
#endif

typedef unsigned short typeLetter;
typedef unsigned short typeAutomata;
typedef int typeDistance;
typedef unsigned int typePair;

struct PNode {
	typeLetter letter;
	PNode* next;
	PNode(short _letter, PNode* _next) : letter(_letter), next(_next) {}
};

#define Id(s1, s2) ((s1 > s2)?(((s1 * (s1 + 1))/2) + s2):(((s2 * (s2 + 1))/2) + s1)) //this is how we compute the ids
#define IdNaive(s1, s2, N) ((s1 > s2)?((s1 * N) + s2):((s2 * N) + s1)) //this is how we compute the ids
#define s1fromId(id) ((int)(sqrt((2.0 * id) +1.0) - 0.5));
#define s2fromId(id, s1) (id - ((s1 * (s1 + 1))/2));

void insertToPath(typeLetter letter, PNode* &head, PNode* &last) {
	PNode* temp = new PNode(letter, NULL);
	if (head == NULL) {
		head = last = temp;
	} else {
		last = last->next = temp;
	}
}

void printAutomata(typeAutomata* automata, typeAutomata N, short p) {
	cout << "Automata ----------------------" << endl;
	for (short i = 0; i < p; ++i) {
		cout << "letter " << (char)(i + 97) << ":\t";
		for (short j = 0; j < N; ++j) {
			cout << automata[i + p * j] << "\t";
		}
		cout << endl;
	}
}

void printInverseAutomata(typeAutomata* inv_automata_ptrs, typeAutomata* inv_automata, short N, short p) {
	cout << "Inverse Automata --------------" << endl;
	for (short i = 0; i < p; ++i) {
		cout << "letter " << (char)(i + 97) << ":\n";
		for (short j = 0; j <= N; ++j) {
			cout << inv_automata_ptrs[i * (N + 1) + j] << "\t";
		}
		cout << endl;
		for (short j = 0; j < N; ++j) {
			cout << inv_automata[i * N + j] << "\t";
		}
		cout << endl << endl;
	}
}

typeAutomata applyPath(typeAutomata* a, PNode* path, typeAutomata N, short P) {
	typeAutomata* actives = new typeAutomata[N];
	for (short i = 0; i < N; ++i) {
		actives[i] = i;
	}

	PNode* pnode = path;
	while (pnode) {
		short let = pnode->letter;
		for (short i = 0; i < N; i++) {
			actives[i] = a[let + actives[i] * P];
		}
		pnode = pnode->next;
	}

	int* active_marker = new int[N];
	for (short i = 0; i < N; i++) {
		active_marker[i] = 0;
	}

	typeAutomata active_count = 0;
	for (short i = 0; i < N; i++) {
		int act = actives[i];
		if (active_marker[act] != 1) {
			active_marker[act] = 1;
			active_count++;
		}
	}

	free(active_marker);
	free(actives);
	return active_count;
}

void pathPrinter(typeAutomata* a, PNode* path, typeAutomata N, short P) {
	PNode* pnode = path;
	unsigned long long int plength = 0;

	//cout << "Path is found: ";
	while (pnode) {
		//cout << pnode->letter << " ";
		pnode = pnode->next;
		plength++;
	}
	cout << endl << "Path length is " << plength << endl;
	cout << "no remaining actives is: " << applyPath(a, path, N, P) << endl << endl;
#if LOG_LEVEL & TIME_ANALYSIS
	out << plength << " ";
#endif
}

#endif