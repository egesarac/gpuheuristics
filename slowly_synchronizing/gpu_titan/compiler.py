#!/usr/bin/python
import os
import sys

            
def main():
    for l in range(2):
        os.chdir("/home/esarac/besh/gpu_titan/all-ThreadLevel-v" + str(l))
        cmd = "make -B"
        retval = os.system(cmd + " > /dev/null")
        if retval != 0:
            exit(1)
            
    for l in [0,1,3]:
        os.chdir("/home/esarac/besh/gpu_titan/all-WarpLevel-v" + str(l))
        cmd = "make -B"
        retval = os.system(cmd + " > /dev/null")
        if retval != 0:
            exit(1)
                
if __name__ == "__main__":
    main()